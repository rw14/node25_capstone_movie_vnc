FROM node:18
# image -- version//
#app directory
WORKDIR /user/src/app
COPY package*.json ./
#all file name package.json will be copy
RUN npm install
#thu vien trong nen phai chay npm 
COPY . . 
RUN npm run build
EXPOSE 8080 
#su dung port tren docker
CMD [ "npm","run","start:prod"]